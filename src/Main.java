import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        while (true) {
            int steps, mode;
            Scanner scanner = new Scanner(System.in);
            System.out.printf("How many steps would you like to transpose: ");
            steps = scanner.nextInt();
            scanner.nextLine();
            System.out.printf("# or b (1,2): ");
            mode = scanner.nextInt();
            scanner.nextLine();
            System.out.printf("Enter chords to transpose e.g 'Em,C#,G': ");
            ChordCollection chords = new ChordCollection(scanner.nextLine());
            Transposer transposer = new Transposer(steps, mode, chords);
            transposer.transpose();
        }

    }

}
