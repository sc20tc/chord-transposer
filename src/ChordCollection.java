import java.util.*;

public class ChordCollection{
    private ArrayList<Chord> chords;

    public ChordCollection() {
        chords = new ArrayList<>();
    }

    public ChordCollection(String s) {
        chords = new ArrayList<>();
        String[] parts = s.split(",");
        for (int i = 0; i < parts.length; i++) {
            Chord chord = new Chord(parts[i].replaceAll("\\s",""));
            chords.add(chord);
        }
    }

    public void print_chords() {
        for (int i = 0; i < chords.size(); i++) {
            System.out.printf("%s%s", chords.get(i).getBass(), chords.get(i).getSubstring());
            if (i != chords.size() - 1) {
                System.out.printf(", ");
            }
        }
        System.out.println("");
    }

    public int size() {
        return chords.size();
    }

    public Chord get(int index) {
        return chords.get(index);
    }

    public void add(Chord c) {
        chords.add(c);
    }

    public void mode(int m) {
        String[][] bass_array = Chord.bass_array();
        if (m == 2) {
            for(int i = 0; i < this.size(); i++) {
                for(int j = 0; j < bass_array.length; j++) {
                    if (chords.get(i).getBass().toLowerCase().equals(bass_array[j][0].toLowerCase()) && !bass_array[j][1].equals("0000")) {
                        Chord new_chord = new Chord(bass_array[j][1] + chords.get(i).getSubstring());
                        chords.set(i, new_chord);
                    }
                }

            }
        }

    }

}
