import java.util.Locale;

public class Chord {

    private String bass;
    private String substring;

    public Chord() {
        bass = "";
        substring = "";
    }

    public Chord(String chord) {
        String[] parsed = parse_chord(chord);
        boolean isChord = isChord(parsed);
        if (isChord) {
            bass = parsed[0];
            substring = parsed[1];
        }
        else {
            System.out.println("No chord exists with bass " + parsed[0] + ".");
            System.exit(0);
        }
    }

    public static String[][] bass_array() {
        String[][] bass = {{"A", "0000"}, {"A#", "Bb"}, {"B", "0000"}, {"C", "0000"}, {"C#", "Db"}, {"D", "0000"},
                {"D#", "Eb"}, {"E", "0000"}, {"F", "0000"}, {"F#", "Gb"}, {"G", "0000"}, {"G#", "Ab"}};
        return bass;
    }

    public String getBass() {return bass; }

    public String getSubstring() {return substring; }

    public int getLength() {
        return bass.length() + substring.length();
    }

    public String[] parse_chord(String chord) {
        chord.toLowerCase();
        String substring = "";
        String[] parsed = new String[2];
        String temp_bass = "";

        if (chord.length() > 1) {
            if (Character.toString(chord.charAt(1)).equals("#") || Character.toString(chord.charAt(1)).equals("b")) {
                temp_bass = Character.toString(chord.charAt(0)).toUpperCase() + Character.toString(chord.charAt(1)).toLowerCase();
                substring = chord.substring(2).toLowerCase();
            }
            else {
                temp_bass = Character.toString(chord.charAt(0)).toUpperCase();
                substring = chord.substring(1).toLowerCase();
            }
        }
        else if (chord.length() == 1) {
            temp_bass = chord.toUpperCase();
        }
        parsed[0] = temp_bass;
        parsed[1] = substring;
        return parsed;
    }

    public boolean isChord(String[] parsed) {
        String[][] bassnotes = bass_array();
        for (int i = 0; i < bassnotes.length; i++) {
            if (parsed[0].toLowerCase().equals(bassnotes[i][0].toLowerCase()) || parsed[0].toLowerCase().equals(bassnotes[i][1].toLowerCase())) {
                return true;
            }
        }
        return false;
    }

}
