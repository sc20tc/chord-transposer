import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.lang.*;

public class Transposer{

    private ChordCollection chords;
    private int steps;
    private int mode;

    public Transposer() {
        chords = new ChordCollection();
    }

    public Transposer(int steps, int mode, ChordCollection chords) {
        this.chords = chords;
        this.steps = steps;
        this.mode = mode;
    }


    // prints a formatted list of chords

    // 1 = #, 2 = b
    public ChordCollection shift() {
        ChordCollection final_chords = new ChordCollection();
        String[][] bass_array = Chord.bass_array();
        for (int i = 0; i < chords.size(); i++) {
            Chord chord = chords.get(i);
            String bass = chord.getBass().toLowerCase().replaceAll("\\s","");
            String substring = chord.getSubstring().replaceAll("\\s","");

            for (int j = 0; j < bass_array.length; j++) {
                int index;
                if (bass.equals(bass_array[j][1].toLowerCase()) || bass.equals(bass_array[j][0].toLowerCase())) {
                    if (j+steps < 0) { index = 12 - ((Math.abs(j+steps)) % 12); }
                    else { index = (j + steps) % 12; }
                    if (index == 12) {index = 0; }
                    bass = bass_array[index][0];
                    bass = bass + substring;
                    break;
                }
            }
            Chord new_chord = new Chord(bass);
            final_chords.add(new_chord);
        }
        final_chords.mode(mode);
        return final_chords;
    }

    public void transpose() {
        System.out.printf("Original chords: ");
        chords.print_chords();
        System.out.printf("Transposed chords: ");
        this.shift().print_chords();
        System.out.println("");
    }

}